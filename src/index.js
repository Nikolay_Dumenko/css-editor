import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import { createLogger } from 'redux-logger';

import reducer from './reducers';
import Routes from './routes';


// store
const node = document.getElementById('root');
const middleware = [];
let store = null;

const history = createHistory();

middleware.push(routerMiddleware(history));

if (process.env.NODE_ENV === 'production') {
  store = createStore(reducer, applyMiddleware(...middleware));
} else {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const logger = createLogger();

  middleware.push(logger);
  store = createStore(reducer, composeEnhancers(applyMiddleware(...middleware)));
}

function App() {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Routes />
      </ConnectedRouter>
    </Provider>
  );
}

render(<App />, node);
