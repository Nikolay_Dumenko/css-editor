export const URLS = {
  EDITOR: '/',
};

export const NAVS = [
  { path: URLS.EDITOR, name: 'editor' },
];
