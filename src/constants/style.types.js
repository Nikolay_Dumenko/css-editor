const prefix = 'style';

export const SET_STYLE = `${prefix}/SET_STYLE`;
export const SET_STATE = `${prefix}/SET_STATE`;
export const DELETE_PROPERTY = `${prefix}/DELETE_PROPERTY`;
export const DELETE_SELECTOR = `${prefix}/DELETE_SELECTOR`;
export const CLEAR_STYLE = `${prefix}/CLEAR_STYLE`;
