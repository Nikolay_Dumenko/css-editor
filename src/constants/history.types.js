const prefix = 'history';

export const UNDO = `${prefix}/UNDO`;
export const REDO = `${prefix}/REDO`;
export const JUMP = `${prefix}/JUMP`;
export const REMOVE = `${prefix}/REMOVE`;
export const SET_LIMIT = `${prefix}/SET_LIMIT`;
export const CLEAR_CACHE = `${prefix}/CLEAR_CACHE`;
