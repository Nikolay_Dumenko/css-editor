// pseudo-classes
const HOVER = ':hover';
const ACTIVE = ':active';
const FOCUS = ':focus';
const EMPTY = ':empty';
const LINK = ':link';
const VISITED = ':visited';
const VALID = ':valid';
const INVALID = ':invalid';
const OPTIONAL = ':optional';
const DISABLED = ':disabled';

// pseudo-element
const BEFORE = '::before';
const AFTER = '::after';
const FIRST_LETTER = '::first-letter';
const PLACEHOLDER = '::placeholder';
const SECTION = '::section';

// combo
const HOVER_BEFORE = HOVER + BEFORE;
const HOVER_AFTER = HOVER + AFTER;
const ACTIVE_BEFORE = ACTIVE + BEFORE;
const ACTIVE_AFTER = ACTIVE + AFTER;

export const TYPE_ATTR = {
  text: 'text',
  checkbox: 'checkbox',
  file: 'file',
};

export const ELEMENTS = {
  div: {
    isContent: true,
    attr: [],
    states: [
      EMPTY,
      SECTION,
      HOVER,
      ACTIVE,
      BEFORE,
      AFTER,
      FIRST_LETTER,
      HOVER_BEFORE,
      HOVER_AFTER,
      ACTIVE_BEFORE,
      ACTIVE_AFTER,
    ],
  },

  button: {
    isContent: true,
    attr: [],
    states: [
      HOVER,
      ACTIVE,
    ],
  },

  a: {
    isContent: true,
    attr: [{ name: 'href', type: TYPE_ATTR.text, value: '' }],
    states: [
      HOVER,
      ACTIVE,
      EMPTY,
      SECTION,
      LINK,
      VISITED,
      BEFORE,
      AFTER,
      FIRST_LETTER,
      HOVER_BEFORE,
      HOVER_AFTER,
      ACTIVE_BEFORE,
      ACTIVE_AFTER,
    ],
  },

  img: {
    isContent: false,
    attr: [
      { name: 'src', type: TYPE_ATTR.file, value: null },
      { name: 'alt', type: TYPE_ATTR.text, value: '' },
    ],
    states: [
      HOVER,
      ACTIVE,
    ],
  },

  input: {
    isContent: false,
    attr: [
      // TODO: add pattern
      { name: 'autoFocus', type: TYPE_ATTR.checkbox, value: false },
      { name: 'disabled', type: TYPE_ATTR.checkbox, value: false },
      { name: 'required', type: TYPE_ATTR.checkbox, value: false },
      { name: 'readOnly', type: TYPE_ATTR.checkbox, value: false },
      { name: 'name', type: TYPE_ATTR.text, value: 'name' },
      { name: 'maxLength', type: TYPE_ATTR.text, value: '200' },
      { name: 'placeholder', type: TYPE_ATTR.text, value: 'placeholder' },
      { name: 'title', type: TYPE_ATTR.text, value: '' },
      { name: 'accessKey', type: TYPE_ATTR.text, value: '' },
    ],
    states: [
      FOCUS,
      HOVER,
      VALID,
      INVALID,
      OPTIONAL,
      DISABLED,
      PLACEHOLDER,
    ],
  },
};

export const UNITS = {
  per: '%',
  rem: 'rem',
  vh: 'vh',
  vw: 'vw',
  em: 'em',
  ch: 'ch',
  ms: 'ms',
  px: 'px',
  s: 's',
};

export const REGEX = {
  VALUE: /\d+(s|ms|px|ch|em|rem|vw|vh|%)/g,
  UNIT: /s|ms|px|ch|em|rem|vw|vh|%/,
};
