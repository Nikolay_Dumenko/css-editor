const prefix = 'elements';

export const SET_ELEMENT = `${prefix}/SET_ELEMENT`;
export const SET_CONTENT = `${prefix}/SET_CONTENT`;
export const SET_CLASS = `${prefix}/SET_CLASS`;
export const SET_ATTR = `${prefix}/SET_ATTR`;
export const TOGGLE_STATE = `${prefix}/TOGGLE_STATE`;
export const TOGGLE_ALL_STATE = `${prefix}/TOGGLE_ALL_STATE`;
