const prefix = 'animations';

export const ADD_ANIMATION = `${prefix}/ADD_ANIMATION`;
export const CREATE_ANIMATION = `${prefix}/CREATE_ANIMATION`;
export const DELETE_ANIMATION = `${prefix}/DELETE_ANIMATION`;
export const TOGGLE_ACTIVE_ANIMATION = `${prefix}/TOGGLE_ACTIVE_ANIMATION`;

// options
export const SET_ANIMATION_NAME = `${prefix}/SET_ANIMATION_NAME`;
export const SET_ANIMATION_DELAY = `${prefix}/SET_ANIMATION_DELAY`;
export const SET_ANIMATION_DURATION = `${prefix}/SET_ANIMATION_DURATION`;
export const SET_ANIMATION_DIRECTION = `${prefix}/SET_ANIMATION_DIRECTION`;
export const SET_ANIMATION_FILL_MODE = `${prefix}/SET_ANIMATION_FILL_MODE`;
export const SET_ANIMATION_TIMING_FUNCTION = `${prefx}/SET_ANIMATION_TIMING_FUNCTION`;
export const SET_ANIMATION_ITERATION_COUNT = `${prefx}/SET_ANIMATION_ITERATION_COUNT`;

// keyframe
export const CREATE_KEYFRAME = `${prefix}/CREATE_KEYFRAME`;
export const UPDATE_KEYFRAME = `${prefix}/UPDATE_KEYFRAME`;
export const DELETE_KEYFRAME = `${prefix}/DELETE_KEYFRAME`;

export const SET_KEYFRAME_STYLE = `${prefix}/SET_KEYFRAME_STYLE`;
