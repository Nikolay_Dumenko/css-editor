import { bindActionCreators } from 'redux';
import { element as types } from 'constants';
import makeActionCreators from 'utils/makeActionCreators';


export const setElement = makeActionCreators(types.SET_ELEMENT, 'element', 'attr', 'states');
export const setClass = makeActionCreators(types.SET_CLASS, 'className');
export const setContent = makeActionCreators(types.SET_CONTENT, 'content');
export const setAttr = makeActionCreators(types.SET_ATTR, 'id', 'value');
export const toggleState = makeActionCreators(types.TOGGLE_STATE, 'id');
export const toggleAllState = makeActionCreators(types.TOGGLE_ALL_STATE, 'active');


export const containerActions = dispatch => bindActionCreators({
  setElement,
  setClass,
  setContent,
  setAttr,
  toggleState,
  toggleAllState,
}, dispatch);
