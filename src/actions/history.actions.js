import { bindActionCreators } from 'redux';
import { history as types } from 'constants';
import makeActionCreators from 'utils/makeActionCreators';


export const undo = makeActionCreators(types.UNDO);
export const redo = makeActionCreators(types.REDO);
export const jump = makeActionCreators(types.JUMP, 'index');
export const setLimit = makeActionCreators(types.SET_LIMIT, 'limit');
export const clearCache = makeActionCreators(types.CLEAR_CACHE);


export const containerActions = dispatch => bindActionCreators({
  undo,
  redo,
  jump,
  setLimit,
  clearCache,
}, dispatch);
