import { bindActionCreators } from 'redux';
import { style as types } from 'constants';
import makeActionCreators from 'utils/makeActionCreators';


/**
 * TODO: testing this actions
 */
export const setStyle = makeActionCreators(types.SET_STYLE, 'selector', 'property', 'value');
export const setState = makeActionCreators(types.SET_STATE, 'type', 'use');
export const deleteProperty = makeActionCreators(types.DELETE_PROPERTY, 'selector', 'property');
export const deleteSelector = makeActionCreators(types.DELETE_SELECTOR, 'selector');
export const clearStyle = makeActionCreators(types.CLEAR_STYLE);


export const containerActions = dispatch => bindActionCreators({
  setStyle,
  setState,
  deleteProperty,
  deleteSelector,
  clearStyle,
}, dispatch);
