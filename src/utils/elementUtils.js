import { other as constants } from 'constants';

const { ELEMENTS } = constants;

export function hasContent(element) {
  if (!ELEMENTS.hasOwnProperty(element)) throw new Error(`${element} is not property`);

  return ELEMENTS[element].isContent;
}

export function getElements() {
  return Object.keys(ELEMENTS);
}
