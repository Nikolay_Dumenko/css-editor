/**
 * @file utils/parseValue
 * @author Nikolay Dumenko
 *
 * @function parseValue
 * @param {string} value
 * @returns {object}
 */

import { other } from 'constants';


export default function (value) {
  const { VALUE, UNIT } = other.REGEX;
  if (value.search(VALUE) === -1) throw new TypeError('Value is not valid');

  const number = Number(value.match(/\d+/)[0]);
  const unit = value.match(UNIT)[0];

  return { number, unit };
}
