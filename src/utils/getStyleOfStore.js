function parseSelector(selector) {
  const arr = selector.split(/::|:/);
  let state = '';

  if (arr[1] && arr[2]) {
    state = `:${arr[1]}::${arr[2]}`;
  } else if (arr[1]) {
    state = `:${arr[1]}`;
  }

  return {
    state,
  };
}

// TODO: fix :
/**
 * @file utils/getStyleOfStore.js
 * @author Nikolay Dumenko
 * @function getStyleOfStore
 * @param {string object}
 * @returns {object}
 */

export default function (element, style) {
  const keys = Object.keys(style);

  return keys.reduce((styles, state) => {
    if (state.indexOf(element) < 0) return styles;

    const selector = parseSelector(state);
    if (state === element) return { ...styles, ...style[state] };

    return {
      ...styles,
      [selector.state]: { ...style[state] },
    };
  }, {});
}
