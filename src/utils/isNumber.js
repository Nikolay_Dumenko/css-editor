/**
 * @file utils/isNumber.js
 * @author Nikolay Dumenko
 * @function isNumber
 * @param {*} n
 * @returns {boolean}
 */

export default function isNumeric(n) {
  return !Number.isNaN(parseFloat(n)) && Number.isFinite(n);
}
