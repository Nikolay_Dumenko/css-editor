/**
 * @file utils/makeReducer
 * @author Nikolay Dumenko
 *
 * @function makeReducer
 * @param {*} initialState
 * @param {*} actionHandlers
 */

export default function (initialState, actionHandlers) {
  return function reducer(state = initialState, action) {
    const { type, payload } = action;

    if (actionHandlers.hasOwnProperty(type)) return actionHandlers[type](state, payload);
    return state;
  };
}
