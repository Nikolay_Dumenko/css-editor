export default (type, ...names) => (...args) => {
  const action = { type };
  if (names.length > 0) action.payload = {};

  names.forEach((item, i) => {
    action.payload[item] = args[i];
  });

  return action;
};
