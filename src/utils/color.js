const color = {};

/**
 * @function hsv2rgb
 * @param {object} hsv h = [0, 360], g = [0, 1], b = [0, 1]
 * @return {object}
 */
color.hsv2rgb = function hsv2rgb(hsv) {
  if (typeof hsv !== 'object') throw new TypeError('hsv is not object');

  const { h, s, v } = hsv;
  const C = v * s;
  const X = C * (1 - Math.abs(((h / 60) % 2) - 1));
  const m = v - C;
  const rgb = {};

  if (h >= 0 && h < 60) {
    rgb.r = C;
    rgb.g = X;
    rgb.b = 0;
  } else if (h >= 60 && h < 120) {
    rgb.r = X;
    rgb.g = C;
    rgb.b = 0;
  } else if (h >= 120 && h < 180) {
    rgb.r = 0;
    rgb.g = C;
    rgb.b = X;
  } else if (h >= 180 && h < 240) {
    rgb.r = 0;
    rgb.g = X;
    rgb.b = C;
  } else if (h >= 240 && h < 300) {
    rgb.r = X;
    rgb.g = 0;
    rgb.b = C;
  } else if (h >= 300 && h <= 360) {
    rgb.r = C;
    rgb.g = 0;
    rgb.b = X;
  }

  return {
    r: Math.round((rgb.r + m) * 255),
    g: Math.round((rgb.g + m) * 255),
    b: Math.round((rgb.b + m) * 255),
  };
};

/**
 * @function rgb2hsv
 * @param {object} rgb r = [0, 255], g = [0, 255] b = [0, 255]
 * @return {object}
 */
color.rgb2hsv = function rgb2hsv(rgb) {
  const { r, g, b } = rgb;
  const dR = r / 255;
  const dG = g / 255;
  const dB = b / 255;

  const cMax = Math.max(dR, dG, dB);
  const cMin = Math.min(dR, dG, dB);
  const d = cMax - cMin;
  const hsv = {
    h: 0,
    s: 0,
    v: cMax,
  };

  if (d > 0) {
    if (cMax === dR) {
      hsv.h = Math.round(60 * (((dG - dB) / d) % 6));
    } else if (cMax === dG) {
      hsv.h = Math.round(60 * ((dB - dR) / d + 2));
    } else if (cMax === dB) {
      hsv.h = Math.round(60 * ((dR - dG) / d + 4));
    }
  }

  if (hsv.h < 0) hsv.h = Math.round(360 + hsv.h);

  if (cMax !== 0) {
    hsv.s = d / cMax;
  }

  return hsv;
};

/**
 * @function rgb2string
 * @param {object} rgb
 * @return {string}
 */
color.rgb2string = function rgb2string(rgb) {
  if (
    typeof rgb !== 'object' ||
    !rgb.hasOwnProperty('r') ||
    !rgb.hasOwnProperty('g') ||
    !rgb.hasOwnProperty('b')
  ) {
    throw new Error('param rgb is not valid');
  }

  if (rgb.hasOwnProperty('a')) {
    return `rgb(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a})`;
  }

  return `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`;
};

color.rgb2hex = function rgb2hex(rgb) {
  const { r, g, b } = rgb;

  return `#${r.toString(16)}${g.toString(16)}${b.toString(16)}`;
};

color.hex2rgb = function hex2rgb(hex) {
  const { r, g, b } = this.getHexOfString(hex);

  return {
    r: parseInt(r, 16),
    g: parseInt(g, 16),
    b: parseInt(b, 16),
  };
};

color.getHexOfString = function getHexOfString(hex) {
  // TODO add test

  return {
    r: hex[0] + hex[1],
    g: hex[2] + hex[3],
    b: hex[4] + hex[5],
  };
};

export default color;
