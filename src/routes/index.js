import React from 'react';
import { Route, Switch, Redirect } from 'react-router';

// pages
import EditorPage from 'containers/pages/EditorPage';
// test
import TestUI from 'containers/pages/TestUI';

import { URLS } from 'config';
// import PrivateRoute from './PrivateRoute';


export default () => (
  <Switch>
    <Route exact path={URLS.EDITOR} component={EditorPage} />
    <Route exact path="/test" component={TestUI} />
    {/* <PrivateRoute exact path={URLS.VERIFICATION} component={VerificationPage} /> */}
    <Redirect to={URLS.EDITOR} />
  </Switch>
);
