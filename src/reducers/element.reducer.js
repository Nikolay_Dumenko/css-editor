import { style, element, other } from 'constants';
import makeReducer from 'utils/makeReducer';


// TODO: delete
// const animation = {
//   id: 0,
//   animationName: '',
//   animationDuration: '',
//   animationTimingFunction: '',
//   animationDelay: '',
//   animationIterationCount: '',
//   animationDirection: '',
//   animationFillMode: '',
//   active: false,
//   keyframes: [
//   TODO: replace on object
//     {
//       keyframe: '10%',
//       style: [{ property: '', value: '' }]
//     }
//   ]
// };

// TODO: delete
const { states } = other.ELEMENTS.input;
const { attr } = other.ELEMENTS.input;


const initialState = {
  elementName: 'input',
  className: '.className',
  content: '',
  attr: attr.map((item, id) => ({ id, ...item })),
  states: states.map((name, id) => ({ id, name, active: false })),
  currentState: { type: 'element', use: { element: 'input', state: '' } },
  style: {
    // test
    div: {
      width: '120px',
      height: '40px',
      fontSize: '12px',
      background: 'blue',
      color: 'white',
      borderRadius: '3px',
    },

    'div:hover': {
      background: 'orange',
    },
  },
  animations: [],
  // TODO: delete
  // const animation = {
  //   id: 0,
  //   animationName: '',
  //   animationDuration: '',
  //   animationTimingFunction: '',
  //   animationDelay: '',
  //   animationIterationCount: '',
  //   animationDirection: '',
  //   animationFillMode: '',
  //   selector: '',
  //   active: false,
  //   keyframes: [
  //   TODO: replace on object
  //     {
  //       keyframe: '10%',
  //       style: [{ property: '', value: '' }]
  //     }
  //   ]
  // };
};

export default makeReducer(initialState, {
  /**
   * ELEMENT
   */

  [element.SET_ELEMENT](state, payload) {
    return {
      ...state,
      currentState: { type: 'element', use: { element: payload.element, state: '' } },
      elementName: payload.element,
      attr: payload.attr.map((item, id) => ({ id, ...item })),
      states: payload.states.map((name, id) => ({ id, name, active: false })),
      style: {},
    };
  },

  [element.SET_CLASS](state, payload) {
    return {
      ...state,
      className: payload.className,
    };
  },

  [element.SET_CONTENT](state, payload) {
    return {
      ...state,
      content: payload.content,
    };
  },

  [element.SET_ATTR](state, payload) {
    return {
      ...state,
      attr: state.attr.map((item) => {
        if (item.id === payload.id) return { ...item, value: payload.value };

        return item;
      }),
    };
  },

  [element.TOGGLE_STATE](state, payload) {
    return {
      ...state,
      states: state.states.map((item) => {
        if (item.id === payload.id) return { ...item, active: !item.active };

        return item;
      }),
    };
  },

  [element.TOGGLE_ALL_STATE](state, payload) {
    return {
      ...state,
      states: state.states.map(item => ({ ...item, active: payload.active })),
    };
  },

  /**
   * STYLE
   */
  [style.SET_STATE](state, payload) {
    return {
      ...state,
      currentState: {
        type: payload.type,
        use: payload.use,
      },
    };
  },

  [style.SET_STYLE](state, payload) {
    return {
      ...state,
      style: {
        ...state.style,
        [payload.selector]: {
          ...state.style[payload.selector],
          [payload.property]: payload.value,
        },
      },
    };
  },

  /**
   * ANIMATIONS
   */
});
