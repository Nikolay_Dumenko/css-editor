/**
 * @file history enhancer
 * @author Nikolay Dumenko
 */

import { history as types } from 'constants';
import moment from 'moment';


function setHistory(state, action, reducer) {
  const {
    past,
    present,
    future,
    logs,
    currentState,
    limit,
  } = state;
  const newPreset = reducer(present, action);

  if (present === newPreset) return state;

  const max = limit - 1;
  const diffIndex = logs.length - future.length;
  const diffLogs = logs.slice(0, diffIndex);

  const newPast = [...past.slice(-(max - 1)), present];
  const newLogs = [...diffLogs.slice(-max), { type: action.type, time: moment().format('HH:mm') }];
  const newCurrentState = (currentState + 1) > max ? max : currentState + 1;

  return {
    ...state,
    currentState: newCurrentState,
    past: newPast,
    present: newPreset,
    future: [],
    logs: newLogs,
  };
}

function undo(state) {
  const {
    past,
    future,
    present,
    currentState,
  } = state;
  const previews = past[past.length - 1];
  const newPast = past.slice(0, past.length - 1);

  return {
    ...state,
    currentState: currentState - 1,
    past: newPast,
    present: previews,
    future: [present, ...future],
  };
}

function redo(state) {
  const {
    past,
    present,
    future,
    currentState,
  } = state;
  const next = future[0];
  const newFuture = future.slice(1);

  return {
    ...state,
    currentState: currentState + 1,
    past: [...past, present],
    present: next,
    future: newFuture,
  };
}

function jump(state, action) {
  const { past, present, future } = state;
  const history = [].concat(past, present, future);

  return {
    ...state,
    currentState: action.index,
    past: history.slice(0, action.index),
    present: history.splice(action.index, 1)[0],
    future: history.slice(action.index),
  };
}

function setLimit(state, action) {
  return { ...state, limit: action.limit };
}

function clearCache(state) {
  const { past, present, logs } = state;

  return {
    ...state,
    past: [],
    present: past[0] || present,
    currentState: 0,
    future: [],
    logs: [logs[0]],
  };
}

export default function (reducer) {
  const initialState = {
    past: [],
    present: reducer(undefined, {}),
    currentState: 0,
    future: [],
    limit: 10,
    logs: [{ type: 'INITIAL', time: moment().format('HH:mm') }],
  };

  return function history(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case types.UNDO:
        return undo(state);

      case types.REDO:
        return redo(state);

      case types.JUMP:
        return jump(state, payload);

      case types.SET_LIMIT:
        return setLimit(state, payload);

      case types.CLEAR_CACHE:
        return clearCache(state);

      default:
        return setHistory(state, action, reducer);
    }
  };
}
