import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';

import history from './history.enhancer';

// reducers
import element from './element.reducer';


export default combineReducers({
  router,
  editor: history(element),
});
