import React, { Component } from 'react';
import ColorPicker from 'containers/modules/ColorPicker';

export default class TestUI extends Component {
  componentDidMount() {
    console.log('test ui');
  }

  render() {
    return (
      <div>
        <ColorPicker />
      </div>
    );
  }
}
