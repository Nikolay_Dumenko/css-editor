/*
  Refactor
*/

import React from 'react';
import HeaderEditor from 'containers/modules/HeaderEditor';
import Viewport from 'containers/modules/Viewport';
import SidebarEditor from 'containers/modules/SidebarEditor';

// test
// import UnitInput from 'components/shared/UnitInput';
// import Select from 'components/shared/Select';
// import { other } from 'constants';

/* const units = [
  { name: other.UNITS.vh },
  { name: other.UNITS.vw },
  { name: other.UNITS.em },
  { name: other.UNITS.rem },
  { name: other.UNITS.px },
  { name: other.UNITS.per },
]; */

/* const selectList = [
  { value: 'relative', name: 'relative' },
  { value: 'absolute', name: 'absolute' },
  { value: 'static', name: 'static' },
]; */

class EditorPage extends React.Component {
  componentDidMount() {
    console.log('editor');
  }

  render() {
    return (
      <div className="editor-page">
        <HeaderEditor />
        <Viewport />
        <SidebarEditor />
      </div>
    );
  }
}

export default EditorPage;
