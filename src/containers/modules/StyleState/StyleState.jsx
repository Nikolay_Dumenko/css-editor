import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import { style } from 'actions';


class StyleState extends Component {
  static propTypes = {
    states: PropTypes.array.isRequired,
    setState: PropTypes.func.isRequired,
  };

  componentDidMount() {
    console.log(this.props.states);
  }

  handleChangeSetState = (e) => {
    const { element, setState } = this.props;
    const { value: state } = e.target;

    setState('element', { element, state });
  };

  renderItems() {
    const { states } = this.props;
    return states.reduce((list, current) => {
      if (current.active) {
        list.push((
          <option value={current.name} key={current.name}>
            {current.name}
          </option>
        ));
      }

      return list;
    }, []);
  }

  render() {
    const { element } = this.props;
    return (
      <div>
        <select onChange={this.handleChangeSetState}>
          <option value="">{element}</option>
          { this.renderItems() }
        </select>
      </div>
    );
  }
}


const selector = createStructuredSelector({
  element: state => state.editor.present.elementName,
  states: state => state.editor.present.states,
});

export default connect(selector, style.containerActions)(StyleState);
