import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import HeaderEditor from 'components/layout/HeaderEditor';
import { getElements } from 'utils/elementUtils';
import { other } from 'constants';
import { element } from 'actions';


class HeaderEditorContainer extends Component {
  static propTypes = {
    setElement: PropTypes.func.isRequired,
  };

  elements = getElements();

  handleClickSetElement = elementName => () => {
    const { setElement } = this.props;
    const { ELEMENTS } = other;
    const { attr, states } = ELEMENTS[elementName];

    setElement(elementName, attr, states);
  };

  render() {
    return (
      <HeaderEditor setElement={this.handleClickSetElement} elements={this.elements} />
    );
  }
}


export default connect(null, element.containerActions)(HeaderEditorContainer);
