import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import ModalWindow from 'components/shared/ModalWindow';
import TextField from 'components/shared/TextField';

import { element as actions } from 'actions';
import { other } from 'constants';
import { hasContent } from 'utils/elementUtils';


class ElementOptions extends Component {
  static propTypes = {
    isShowAllStates: PropTypes.bool.isRequired,
    states: PropTypes.array.isRequired,
    attr: PropTypes.array.isRequired,
    content: PropTypes.string.isRequired,
    element: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,

    setContent: PropTypes.func.isRequired,
    setClass: PropTypes.func.isRequired,
    setAttr: PropTypes.func.isRequired,
    toggleState: PropTypes.func.isRequired,
    toggleAllState: PropTypes.func.isRequired,
  };

  state = {
    isShow: false,
  };

  // handlers
  handleClickShow = () => this.setState({ isShow: true });

  handleClickClose = () => this.setState({ isShow: false });

  handleChangeContent = (e) => {
    const { setContent } = this.props;
    const { value } = e.target;

    setContent(value);
  };

  handleChangeClassName = (e) => {
    const { setClass } = this.props;
    const { value } = e.target;

    setClass(value);
  };

  handleChangeAttrText = id => (e) => {
    const { setAttr } = this.props;
    const { value } = e.target;

    setAttr(id, value);
  };

  handleChangeAttrCheckbox = id => (e) => {
    const { setAttr } = this.props;
    const { checked } = e.target;

    setAttr(id, checked);
  };

  handleChangeAllState = () => {
    const { toggleAllState, isShowAllStates } = this.props;
    toggleAllState(!isShowAllStates);
  };

  handleChangeState = id => () => {
    const { toggleState } = this.props;
    toggleState(id);
  };

  // renders
  renderAttrText(name, value, id) {
    return (
      <div key={id}>
        <TextField value={value} onChange={this.handleChangeAttrText(id)} />
      </div>
    );
  }

  renderAttrCheckbox(name, value, id) {
    return (
      <div key={id}>
        <label>
          <input type="checkbox" checked={value} onChange={this.handleChangeAttrCheckbox(id)} />
          {name}
        </label>
      </div>
    );
  }

  // renderAttrFile(name, id) {
  //   return (
  //     <div key={id}>
  //       <label>
  //         <h6>{name}</h6>
  //         <input type="file" />
  //       </label>
  //     </div>
  //   );
  // }

  renderContent() {
    const { content, element } = this.props;

    if (!hasContent(element)) return null;

    return (
      <div>
        <label>
          <h3>Content</h3>
          <input type="text" value={content} onChange={this.handleChangeContent} />
        </label>
      </div>
    );
  }

  renderClassName() {
    const { className } = this.props;

    return (
      <div>
        <label>
          <h4>Class</h4>
          <input type="text" value={className} onChange={this.handleChangeClassName} />
        </label>
      </div>
    );
  }

  renderAttr() {
    const { attr } = this.props;
    const { TYPE_ATTR } = other;

    if (!attr.length) return null;

    return attr.map((item) => {
      switch (item.type) {
        case TYPE_ATTR.text:
          return this.renderAttrText(item.name, item.value, item.id);

        case TYPE_ATTR.checkbox:
          return this.renderAttrCheckbox(item.name, item.value, item.id);

        // case TYPE_ATTR.file:
        //   return this.renderAttrFile(item.name, item.id);

        default:
          return this.renderAttrText(item.name, item.value, item.id);
      }
    });
  }

  renderStateList() {
    const { isShowAllStates } = this.props;

    return (
      <ul>
        <li>
          <label>
            <input type="checkbox" checked={isShowAllStates} onChange={this.handleChangeAllState} />
            All
          </label>
        </li>
        { this.renderStates() }
      </ul>
    );
  }

  renderStates() {
    const { states } = this.props;
    return states.map(item => (
      <li key={item.id}>
        <label>
          <input
            type="checkbox"
            checked={item.active}
            onChange={this.handleChangeState(item.id)}
          />
          {item.name}
        </label>
      </li>
    ));
  }

  render() {
    const { isShow } = this.state;

    return (
      <Fragment>
        <button type="button" className="options-element-btn" onClick={this.handleClickShow}>
          <img src="images/icons/settings.svg" alt="element" />
        </button>
        <ModalWindow
          title="Настройки елемента"
          show={isShow}
          onClose={this.handleClickClose}
        >
          <div className="space-between">
            <div>
              { this.renderClassName() }
              { this.renderContent() }
              { this.renderAttr() }
            </div>
            <div>
              { this.renderStateList() }
            </div>
          </div>
        </ModalWindow>
      </Fragment>
    );
  }
}


const selector = createStructuredSelector({
  element: state => state.editor.present.elementName,
  className: state => state.editor.present.className,
  content: state => state.editor.present.content,
  attr: state => state.editor.present.attr,
  states: state => state.editor.present.states,
  isShowAllStates: state => state.editor.present.states.every(item => item.active),
});

export default connect(selector, actions.containerActions)(ElementOptions);
