import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { StyleSheet, css } from 'aphrodite';

import getStyleOfStore from 'utils/getStyleOfStore';
import { hasContent } from 'utils/elementUtils';


class Viewport extends Component {
  static propTypes = {
    content: PropTypes.string.isRequired,
    element: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
    style: PropTypes.object.isRequired,
    attr: PropTypes.array.isRequired,
  };

  componentDidMount() {
    console.log('generation', getStyleOfStore(this.props.element, this.props.style));
    this.createStyle();
  }

  getAttr() {
    const { attr } = this.props;
    return attr.reduce((obj, current) => ({
      ...obj,
      [current.name]: current.value,
    }), {});
  }

  createStyle() {
    const { style, element } = this.props;
    const styles = getStyleOfStore(element, style);

    return StyleSheet.create({
      [element]: {
        ...styles,
      },
    });
  }

  createElement() {
    const { element, content } = this.props;
    const style = this.createStyle();
    const attr = this.getAttr();
    const children = hasContent(element) ? content : null;

    return React.createElement(element, {
      className: css(style[element]),
      ...attr,
    }, children);
  }

  renderElement() {
    const { className } = this.props;

    return (
      <div className="viewport-element">
        <div className="viewport-header">
          <span>{ className }</span>
        </div>
        <div className="viewport-body">
          { this.createElement() }
        </div>
      </div>
    );
  }

  // renderState() {}

  render() {
    return (
      <main className="viewport">
        { this.renderElement() }
      </main>
    );
  }
}


const selector = createStructuredSelector({
  content: state => state.editor.present.content,
  element: state => state.editor.present.elementName,
  className: state => state.editor.present.className,
  attr: state => state.editor.present.attr,
  style: state => state.editor.present.style,
});

export default connect(selector)(Viewport);
