import React, { Component } from 'react';
import PropTypes from 'prop-types';


class Opacity extends Component {
  static propTypes = {
    color: PropTypes.object.isRequired,
    value: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  static MIN = 0;
  static MAX = 1;
  static SLIDER_HEIGHT = 200;
  static THUMB_SIZE = 14;

  state = {
    slider: 0,
    thumb: 0,
  };

  slider = React.createRef();
  thumb = React.createRef();

  componentWillMount() {
    this.setState({
      slider: Opacity.SLIDER_HEIGHT - Opacity.THUMB_SIZE,
      thumb: Opacity.THUMB_SIZE / 2,
    });
  }

  getThumbStyle() {
    const { value } = this.props;
    const position = this.value2position(value);

    return {
      top: `${position}px`,
      width: `${Opacity.THUMB_SIZE}px`,
      height: `${Opacity.THUMB_SIZE}px`,
    };
  }

  getFillStyle() {
    const { color } = this.props;

    return {
      background: `linear-gradient(to top, transparent, rgb(${color.r}, ${color.g}, ${color.b}))`,
    };
  }

  getPosition(e) {
    const { thumb } = this.state;
    const rect = this.slider.current.getBoundingClientRect();

    return e.clientY - rect.top - thumb;
  }

  value2position(value) {
    const { slider } = this.state;

    return value / Opacity.MAX * slider;
  }

  position2value(position) {
    const { slider } = this.state;
    const value = position / slider * Opacity.MAX + Opacity.MIN;

    if (value < Opacity.MIN) return Opacity.MIN;
    if (value > Opacity.MAX) return Opacity.MAX;

    return value;
  }

  // handles
  handleMouseUp = () => {
    window.removeEventListener('mousemove', this.handleMouseMove);
    window.removeEventListener('mouseup', this.handleMouseUp);
  }

  handleMouseMove = (e) => {
    const { onChange } = this.props;
    const y = this.getPosition(e);
    const value = this.position2value(y);

    onChange(Number(value.toFixed(2)));
  }

  handleMouseDown = () => {
    window.addEventListener('mousemove', this.handleMouseMove);
    window.addEventListener('mouseup', this.handleMouseUp);
  }

  render() {
    return (
      <div className="opacity-slider" ref={this.slider}>
        <div className="opacity-slider__fill" style={this.getFillStyle()} />
        <div
          ref={this.thumb}
          style={this.getThumbStyle()}
          className="opacity-slider__thumb"
          onMouseDown={this.handleMouseDown}
        />
      </div>
    );
  }
}

export default Opacity;
