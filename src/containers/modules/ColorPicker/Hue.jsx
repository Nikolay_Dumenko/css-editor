import React, { Component } from 'react';
import PropTypes from 'prop-types';


class Hue extends Component {
  static propTypes = {
    value: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  static MIN = 0;
  static MAX = 360;
  static SLIDER_HEIGHT = 200;
  static THUMB_SIZE = 14;

  state = {
    slider: 0,
    thumb: 0,
  };

  slider = React.createRef();
  thumb = React.createRef();

  // getSliderStyle() {
  //   return {
  //     width: `${Hue.SLIDER_WIDTH}px`,
  //   };
  // }

  componentWillMount() {
    this.setState({
      slider: Hue.SLIDER_HEIGHT - Hue.THUMB_SIZE,
      thumb: Hue.THUMB_SIZE / 2,
    });
  }

  getThumbStyle() {
    const { value } = this.props;
    const position = this.value2position(value);

    return {
      top: `${position}px`,
      width: `${Hue.THUMB_SIZE}px`,
      height: `${Hue.THUMB_SIZE}px`,
    };
  }

  getPosition(e) {
    const { thumb } = this.state;
    const rect = this.slider.current.getBoundingClientRect();

    return e.clientY - rect.top - thumb;
  }

  value2position(value) {
    const { slider } = this.state;

    return value / Hue.MAX * slider;
  }

  position2value(position) {
    const { slider } = this.state;
    const value = position / slider * Hue.MAX + Hue.MIN;

    if (value < Hue.MIN) return Hue.MIN;
    if (value > Hue.MAX) return Hue.MAX;

    return value;
  }

  // handles
  handleMouseUp = () => {
    window.removeEventListener('mousemove', this.handleMouseMove);
    window.removeEventListener('mouseup', this.handleMouseUp);
  }

  handleMouseMove = (e) => {
    const { onChange } = this.props;
    const y = this.getPosition(e);
    const value = this.position2value(y);

    onChange(+value);
  }

  handleMouseDown = () => {
    window.addEventListener('mousemove', this.handleMouseMove);
    window.addEventListener('mouseup', this.handleMouseUp);
  }

  render() {
    return (
      <div className="hue-slider" ref={this.slider}>
        <div
          ref={this.thumb}
          style={this.getThumbStyle()}
          className="hue-slider__thumb"
          onMouseDown={this.handleMouseDown}
        />
      </div>
    );
  }
}

export default Hue;
