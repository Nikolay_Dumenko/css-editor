import React, { Component } from 'react';
import PropTypes from 'prop-types';


class ColorArea extends Component {
  static propTypes = {
    defaultColor: PropTypes.object.isRequired,
    color: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  static area = { width: 200, height: 200 };
  static thumb = { width: 16, height: 16 };

  static position2SV(x, y) {
    const { width, height } = ColorArea.area;
    return {
      s: x / width,
      v: 1 - (y / height),
    };
  }

  static SV2position(s, v) {
    const { width, height } = ColorArea.area;
    return {
      x: s * width,
      y: v * height,
    };
  }

  static getPositionThumb(x, y) {
    const { width: wArea, height: hArea } = ColorArea.area;
    const { width: wThumb, height: hThumb } = ColorArea.thumb;
    const position = {
      x: x - wThumb / 2,
      y: y - hThumb / 2,
    };

    if (x < 0) position.x = -(wThumb / 2);
    if (y < 0) position.y = -(hThumb / 2);
    if (x > wArea) position.x = wArea - wThumb / 2;
    if (y > hArea) position.y = hArea - hThumb / 2;

    return position;
  }

  state = {
    position: { x: 0, y: 0 },
  };

  area = React.createRef();
  thumb = React.createRef();

  componentWillMount() {
    const { defaultColor } = this.props;
    const { x, y } = ColorArea.SV2position(defaultColor.s, defaultColor.v);
    const position = ColorArea.getPositionThumb(x, y);

    this.setState({ position });
  }

  getAreaStyle() {
    const { color } = this.props;
    const { width, height } = ColorArea.area;
    return {
      width: `${width}px`,
      height: `${height}px`,
      backgroundColor: color,
    };
  }

  getThumbStyle() {
    const { position } = this.state;
    const { width, height } = ColorArea.thumb;
    return {
      width: `${width}px`,
      height: `${height}px`,
      left: `${position.x}px`,
      top: `${position.y}px`,
    };
  }

  getPosition(e) {
    const rect = this.area.current.getBoundingClientRect();
    const position = {
      x: e.clientX - rect.left,
      y: e.clientY - rect.top,
    };

    if (position.x < 0) position.x = 0;
    if (position.y < 0) position.y = 0;
    if (position.x > rect.width) position.x = rect.width;
    if (position.y > rect.height) position.y = rect.height;

    return position;
  }

  handleMouseDown = () => {
    window.addEventListener('mousemove', this.handleMouseMove);
    window.addEventListener('mouseup', this.handleMouseUp);
  };

  handleMouseMove = (e) => {
    e.preventDefault();
    const { onChange } = this.props;
    const { x, y } = this.getPosition(e);
    const position = ColorArea.getPositionThumb(x, y);

    this.setState({ position });
    onChange(ColorArea.position2SV(x, y));
  };

  handleMouseUp = () => {
    window.removeEventListener('mousemove', this.handleMouseMove);
    window.removeEventListener('mouseup', this.handleMouseUp);
  };

  render() {
    const areaStyle = this.getAreaStyle();
    const thumbStyle = this.getThumbStyle();

    return (
      <div
        ref={this.area}
        style={areaStyle}
        className="color-area"
        onMouseDown={this.handleMouseDown}
      >
        <div
          ref={this.thumb}
          style={thumbStyle}
          className="color-area__thumb"
        />
      </div>
    );
  }
}

export default ColorArea;
