import React, { Component } from 'react';

import color from 'utils/color';
import ColorArea from './ColorArea';
import Hue from './Hue';
import Opacity from './Opacity';


class ColorPicker extends Component {
  state = {
    opacity: 1,
    hsv: { h: 120, s: 1, v: 0 },
    rgb: { r: 0, g: 0, b: 0 },
  };

  getColorArea() {
    const { h: hue } = this.state.hsv;
    const rgb = color.hsv2rgb({ h: hue, s: 1, v: 1 });

    return color.rgb2string(rgb);
  }

  // handles
  handleChangeArea = ({ s, v }) => {
    const { h } = this.state.hsv;
    const hsv = { h, s, v };
    const rgb = color.hsv2rgb({ h, s, v });

    this.setState({ rgb, hsv });
  };

  handleChangeHue = (h) => {
    const { v, s } = this.state.hsv;
    const hsv = { h, s, v };
    const rgb = color.hsv2rgb({ h, s, v });

    this.setState({ hsv, rgb });
  };

  handleChangeOpacity = opacity => this.setState({ opacity });

  // renders
  renderRGBinput() {
    const { rgb } = this.state;

    return (
      <div>
        <div>
          <label>
            R
            <input type="text" value={rgb.r} readOnly />
          </label>
        </div>
        <div>
          <label>
            G
            <input type="text" value={rgb.g} readOnly />
          </label>
        </div>
        <div>
          <label>
            B
            <input type="text" value={rgb.b} readOnly />
          </label>
        </div>
        <div>
          <label>
            HEX
            <input type="text" value={color.rgb2hex(rgb)} readOnly />
          </label>
        </div>
      </div>
    );
  }

  render() {
    const { rgb, hsv, opacity } = this.state;
    const style = {
      backgroundColor: color.rgb2string(rgb),
    };

    return (
      <div className="color-picker">
        <div className="space-between">
          <ColorArea
            onChange={this.handleChangeArea}
            defaultColor={{ s: hsv.s, v: hsv.v }}
            color={this.getColorArea()}
          />
          <Hue
            value={hsv.h}
            onChange={this.handleChangeHue}
          />
          <Opacity
            color={rgb}
            value={opacity}
            onChange={this.handleChangeOpacity}
          />
        </div>
        {/* { this.renderRGBinput() } */}
        <br/>
        <div style={style}>
          color
        </div>
      </div>
    );
  }
}

export default ColorPicker;
