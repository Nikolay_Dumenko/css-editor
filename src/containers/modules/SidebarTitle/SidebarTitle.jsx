import React, { Component } from 'react';
import PropTypes from 'prop-types';


class SidebarTitle extends Component {
  static propTypes = {
    title: PropTypes.string,
  };

  static defaultProps = {
    title: 'title',
  };

  componentDidMount() {
    console.log('title');
  }

  render() {
    const { title } = this.props;

    return (
      <div className="sidebar-title">
        <h4>{ title }</h4>
        <div>
          <button className="sidebar-title__icon"><img src="images/icons/info.svg" alt="icon"/></button>
          <button className="sidebar-title__icon"><img src="images/icons/code.svg" alt="icon"/></button>
          <button className="sidebar-title__icon"><img src="images/icons/settings.svg" alt="icon"/></button>
        </div>
      </div>
    );
  }
}

export default SidebarTitle;
