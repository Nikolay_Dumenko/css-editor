import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { style as actions } from 'actions';

import Sidebar from 'components/layout/Sidebar';
import SizePanel from './components/SizePanel';


class SidebarEditor extends Component {
  static propTypes = {
    currentState: PropTypes.object.isRequired,
    style: PropTypes.object.isRequired,
    setStyle: PropTypes.func.isRequired,
  };

  state = {
    active: 0,
  };

  // handlers
  handleChangeTab = active => () => this.setState({ active });

  // renders
  renderSizePanel() {
    const { style, currentState, setStyle } = this.props;
    const selector = currentState.use.element + currentState.use.state;

    return (
      <SizePanel
        styleOfStore={style}
        selector={selector}
        setStyle={setStyle}
      />
    );
  }

  render() {
    const { active } = this.state;
    const panels = [
      { panel: 'text', tab: 'images/icons/text_fields.svg' },
      { panel: this.renderSizePanel(), tab: 'images/icons/zoom_out_map.svg' },
      { panel: 'background', tab: 'images/icons/color_lens.svg' },
      { panel: 'border', tab: 'images/icons/border_all.svg' },
      { panel: 'shadow', tab: 'images/icons/texture.svg' },
      { panel: 'transform', tab: 'images/icons/transform.svg' },
      { panel: 'filter', tab: 'images/icons/photo_filter.svg' },
      { panel: 'transitions', tab: 'images/icons/av_timer.svg' },
    ];

    return (
      <Sidebar
        onChange={this.handleChangeTab}
        active={active}
        tabs={panels}
      />
    );
  }
}


const selector = createStructuredSelector({
  currentState: state => state.editor.present.currentState,
  style: state => state.editor.present.style,
});

export default connect(selector, actions.containerActions)(SidebarEditor);
