import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { other } from 'constants';
import SidebarTitle from 'containers/modules/SidebarTitle';
import UnitInput from 'components/shared/UnitInput';

// TEST
import StyleState from 'containers/modules/StyleState';


const units = [
  { name: other.UNITS.px },
  { name: other.UNITS.per },
  { name: other.UNITS.em },
  { name: other.UNITS.vh },
  { name: other.UNITS.vw },
  { name: other.UNITS.rem },
];

class SizePanel extends Component {
  static propTypes = {
    selector: PropTypes.string.isRequired,
    styleOfStore: PropTypes.object.isRequired,
    setStyle: PropTypes.func.isRequired,
  };

  componentDidMount() {
    console.log('size panel');
  }

  hasSelector(property) {
    const { styleOfStore, selector } = this.props;
    return styleOfStore.hasOwnProperty(selector) && styleOfStore[selector].hasOwnProperty(property);
  }

  getStyle(property, defaultValue) {
    const { styleOfStore, selector } = this.props;

    if (!this.hasSelector(property)) return defaultValue;

    return styleOfStore[selector][property];
  }

  handleChangeStyle = property => (value) => {
    const { selector } = this.props;
    this.props.setStyle(selector, property, value);
  };

  renderWidth() {
    const style = this.getStyle('width', '15px');

    return (
      <UnitInput
        min={0}
        max={300}
        title="width"
        unitList={units}
        value={style}
        onChange={this.handleChangeStyle('width')}
      />
    );
  }

  renderHeight() {
    const style = this.getStyle('height', '15px');

    return (
      <UnitInput
        min={0}
        max={300}
        title="height"
        unitList={units}
        value={style}
        onChange={this.handleChangeStyle('height')}
      />
    );
  }

  renderSize() {
    return (
      <div className="center">
        { this.renderWidth() }
        { this.renderHeight() }
      </div>
    );
  }

  render() {
    return (
      <div>
        <SidebarTitle title="Size" />
        { this.renderSize() }
        <StyleState />
      </div>
    );
  }
}

export default SizePanel;
