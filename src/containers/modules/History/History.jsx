/**
 * @file containers/modules/History/History.jsx
 * @author Nikolay Dumenko
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { history as actions } from 'actions';


class History extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    undo: PropTypes.func.isRequired,
    redo: PropTypes.func.isRequired,
    jump: PropTypes.func.isRequired,
    clearCache: PropTypes.func.isRequired,
  };

  state = {
    showHistory: false,
  };

  componentDidMount() {
    window.addEventListener('click', this.handleClickHideHistory);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.handleClickHideHistory);
  }

  // handlers
  handleClickShowHistory = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({ showHistory: true });
  };

  handleClickHideHistory = (e) => {
    const { showHistory } = this.state;

    if (showHistory && !e.target.closest('.history-dialog')) {
      this.setState({ showHistory: false });
    }
  };

  handleClickUndo = () => {
    const { undo } = this.props;
    undo();
  };

  handleClickRedo = () => {
    const { redo } = this.props;
    redo();
  };

  handleClickJump = index => () => {
    const { jump } = this.props;
    jump(index);
  }

  handleClickClearCache = () => {
    const { clearCache } = this.props;
    clearCache();
  }

  // renders
  renderTable() {
    const { history } = this.props;
    return (
      <table className="history__table">
        <thead>
          <tr>
            <th>#</th>
            <th>Actions</th>
            <th>Time</th>
          </tr>
        </thead>
        <tbody>
          {
            history.logs.map((item, i) => (
              <tr
                key={i}
                className={history.currentState === i ? 'history__col--active' : ''}
                onClick={this.handleClickJump(i)}
              >
                <td>{i + 1}</td>
                <td>{item.type}</td>
                <td>{item.time}</td>
              </tr>
            ))
          }
        </tbody>
      </table>
    );
  }

  renderHistory() {
    const { showHistory } = this.state;

    if (!showHistory) return null;

    return (
      <div className="history-dialog show">
        { this.renderTable() }
        { this.renderFooter() }
      </div>
    );
  }

  renderFooter() {
    const { history } = this.props;
    return (
      <div className="history-footer">
        <span>States: {history.currentState + 1}/{history.limit}</span>
        <button
          type="button"
          onClick={this.handleClickClearCache}
          className="history-delete"
        >
          <img src="images/icons/delete.svg" alt="delete" />
          clear history
        </button>
      </div>
    );
  }

  renderBtnUndo() {
    const { history } = this.props;
    const isDisabled = history.past.length === 0;

    return (
      <button
        type="button"
        className="history__button"
        onClick={this.handleClickUndo}
        disabled={isDisabled}
      >
        <img src="images/icons/undo.svg" alt="btn" />
      </button>
    );
  }

  renderBtnRedo() {
    const { history } = this.props;
    const isDisabled = history.future.length === 0;
    return (
      <button
        type="button"
        className="history__button"
        onClick={this.handleClickRedo}
        disabled={isDisabled}
      >
        <img src="images/icons/redo.svg" alt="btn" />
      </button>
    );
  }

  renderBtnHistory() {
    const { showHistory } = this.state;
    return (
      <button
        type="button"
        className={`history__button ${showHistory ? 'history__button--active' : ''}`}
        onClick={this.handleClickShowHistory}
      >
        <img src="images/icons/history.svg" alt="btn" />
      </button>
    );
  }

  render() {
    return (
      <div className="history">
        {/* <h4>cache: { this.props.history.logs.length }</h4>
        <h4>max cache: { this.props.history.maxCache }</h4> */}
        <div className="history-btns">
          { this.renderBtnUndo() }
          { this.renderBtnRedo() }
          { this.renderBtnHistory() }
        </div>

        { this.renderHistory() }
        {/* <button type="button" onClick={this.handleClickClearCache}>
          clear history
        </button> */}
      </div>
    );
  }
}


const selector = createStructuredSelector({
  history: state => state.editor,
});

export default connect(selector, actions.containerActions)(History);
