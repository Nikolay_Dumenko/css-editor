/**
 * @file components/layout/HeaderEditor/HeaderEditor.jsx
 * @author Nikolay Dumenko
 */

import React from 'react';
import PropTypes from 'prop-types';
import Line from 'components/shared/Line';
import History from 'containers/modules/History';
import ElementOptions from 'containers/modules/ElementOptions';


const propTypes = {
  elements: PropTypes.array.isRequired,
  setElement: PropTypes.func.isRequired,
};

function HeaderEditor({ elements, setElement }) {
  function renderElements() {
    return elements.map(item => (
      <li key={item} onClick={setElement(item)}>
        {item}
      </li>
    ));
  }

  return (
    <header className="header-editor">
      <nav className="header-editor-nav">
        <ul className="header-editor-nav__list">
          <li className="header-editor-nav__item">
            Создать
            <ul className="header-editor-dropdown">
              { renderElements() }
            </ul>
          </li>
          <li className="header-editor-nav__item header-editor-nav__item--disabled">Открыть</li>
          <li className="header-editor-nav__item header-editor-nav__item--disabled">Сохранить</li>
          <li className="header-editor-nav__item header-editor-nav__item--disabled">Скачать</li>
        </ul>
      </nav>
      <Line />
      <History />
      <Line />
      <ElementOptions />
    </header>
  );
}

HeaderEditor.propTypes = propTypes;

export default HeaderEditor;
