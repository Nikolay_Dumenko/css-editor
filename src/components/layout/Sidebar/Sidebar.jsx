/**
 * @file components/layout/Sidebar/Sidebar.jsx
 * @author Nikolay Dumenko
 */

import React from 'react';
import PropTypes from 'prop-types';
import Tabs from './Tabs';
import Content from './Content';


const propTypes = {
  active: PropTypes.number.isRequired,
  tabs: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
};

function Sidebar({
  active,
  tabs,
  onChange,
}) {
  return (
    <aside className="sidebar">
      <Content panels={tabs} active={active} />
      <Tabs tabs={tabs} active={active} onChange={onChange} />
    </aside>
  );
}

Sidebar.propTypes = propTypes;

export default Sidebar;
