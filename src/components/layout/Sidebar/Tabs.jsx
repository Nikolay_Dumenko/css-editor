import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
  tabs: PropTypes.array.isRequired,
  active: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

function Tabs({
  tabs,
  active,
  onChange,
}) {
  function renderList() {
    return tabs.map((item, index) => (
      <div
        className={`sidebar-tab ${active === index ? 'sidebar-tab--active' : ''}`}
        onClick={onChange(index)}
        key={index}
      >
        <span>
          <i className="sidebar-tab__icon">
            <img src={item.tab} alt="icon" />
          </i>
          <i className="sidebar-tab__icon">
            <img src={item.tab} alt="icon" />
          </i>
        </span>
      </div>
    ));
  }

  return (
    <div className="sidebar-tabs">
      { renderList() }
    </div>
  );
}

Tabs.propTypes = propTypes;

export default Tabs;
