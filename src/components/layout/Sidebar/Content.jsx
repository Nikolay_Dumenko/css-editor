import React from 'react';
import PropsTypes from 'prop-types';


const propTypes = {
  panels: PropsTypes.array.isRequired,
  active: PropsTypes.number.isRequired,
};

function Content({
  panels,
  active,
}) {
  function renderList() {
    return panels.map((panel, index) => (
      <div className={`sidebar-panel ${active === index ? 'fadeIn' : ''}`} key={index}>
        { panels[index].panel }
      </div>
    ));
  }

  return (
    <div className="sidebar-content">
      {renderList()}
    </div>
  );
}

Content.propTypes = propTypes;

export default Content;
