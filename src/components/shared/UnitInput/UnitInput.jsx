/**
 * @file comoponents/shared/UnitInput/UnitInput.jsx
 * @author Nikolay Dumenko
 */

import React from 'react';
import PropTypes from 'prop-types';
import isNumber from 'utils/isNumber';
import parseValue from 'utils/parseValue';

const propTypes = {
  title: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  unitList: PropTypes.array,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  onChange: PropTypes.func.isRequired,
};

const defaultProps = {
  value: '0px',
  min: null,
  max: null,
  title: null,
  unitList: null,
};

function UnitInput({
  title,
  min,
  max,
  unitList,
  value,
  onChange,
}) {
  const { number, unit } = parseValue(value);
  // console.log(unit, number, typeof number);
  function handleChangeInput(event) {
    const v = Number(event.target.value);

    if (!isNumber(v)) return onChange(number + unit);
    if (min && min > v) return onChange(min + unit);
    if (max && max < v) return onChange(max + unit);

    return onChange(v + unit);
  }

  function handleClickIncrement() {
    if (max && number + 1 > max) return onChange(max + unit);
    return onChange(number + 1 + unit);
  }

  function handleClickDecrement() {
    if (min && number - 1 < min) return onChange(min + unit);
    return onChange((number - 1) + unit);
  }

  function handleClickUnit(u) {
    return () => onChange(number + u);
  }

  function renderTitle() {
    if (!title) return null;

    return (
      <h5 className="ui-unit-input__title">{ title }</h5>
    );
  }

  function renderValue() {
    return (
      <input
        className="ui-unit-input__input"
        type="text"
        value={number}
        onChange={handleChangeInput}
      />
    );
  }

  function renderArrow() {
    return (
      <div className="ui-unit-input__arrows">
        <button
          type="button"
          className="ui-unit-input__arrow"
          onClick={handleClickIncrement}
        >
          <img src="/images/icons/arrow_up.svg" alt="arrow" />
        </button>
        <button
          type="button"
          className="ui-unit-input__arrow"
          onClick={handleClickDecrement}
        >
          <img src="/images/icons/arrow_down.svg" alt="arrow" />
        </button>
      </div>
    );
  }

  function renderSelect() {
    if (!unitList) return null;

    return (
      <div className="ui-unit-input__unit">
        <span>{ unit }</span>
        <ul className="ui-unit-input__list">
          {
            unitList.map(item => (
              <li key={item.name} onClick={handleClickUnit(item.name)}>{item.name}</li>
            ))
          }
        </ul>
      </div>
    );
  }

  return (
    <div className="ui-unit-input-wrapper">
      { renderTitle() }
      <div className="ui-unit-input">
        <div className="ui-unit-input__wrapper">
          { renderValue() }
          { renderArrow() }
        </div>
        { renderSelect() }
      </div>
    </div>
  );
}

UnitInput.propTypes = propTypes;
UnitInput.defaultProps = defaultProps;

export default UnitInput;
