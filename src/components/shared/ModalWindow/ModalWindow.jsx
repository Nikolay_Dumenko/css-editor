import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
  children: PropTypes.any,
  show: PropTypes.bool,
  title: PropTypes.string,
  onClose: PropTypes.func,
};

const defaultProps = {
  children: '',
  title: '',
  show: false,
  onClose: null,
};

function ModalWindow({
  children,
  title,
  show,
  onClose,
}) {
  return (
    <div className="modal-background" hidden={!show}>
      <div className="modal-window">
        <header className="modal-window-header">
          <span>{ title }</span>
          <button type="submit" onClick={onClose} className="modal-window-close">
            <img src="images/icons/close.svg" alt="x" />
          </button>
        </header>
        <div className="modal-window-body">
          { children }
        </div>
      </div>
    </div>
  );
}

ModalWindow.propTypes = propTypes;
ModalWindow.defaultProps = defaultProps;

export default ModalWindow;
