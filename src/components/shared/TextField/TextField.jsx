import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

function TextField({ value, onChange }) {
  return (
    <div className="text-field">
      <input type="text" value={value} onChange={onChange} />
    </div>
  );
}

TextField.propTypes = propTypes;

export default TextField;
