/**
 * @file components/shared/Select/Select.jsx
 * @author Nikolay Dumenko
 */

import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
};

function Select({
  title,
  value,
  list,
  onChange,
}) {
  function getValue() {
    const v = list.find(item => item.value === value);
    if (!v || !v.hasOwnProperty('value')) throw new Error('active is not found');

    return v.value;
  }

  function renderValue() {
    return (
      <span className="ui-select__value">
        { getValue() }
      </span>
    );
  }

  function renderList() {
    return list.map(item => (
      <li onClick={onChange(item.value)} key={item.value}> {item.name} </li>
    ));
  }

  return (
    <div className="ui-select-wrapper">
      <h5 className="ui-select__title">{ title }</h5>
      <div className="ui-select">
        { renderValue() }
        <i className="ui-select__icon">
          <img src="images/icons/arrow_down.svg" alt="" />
        </i>
        <ul className="ui-select__list">
          { renderList() }
        </ul>
      </div>
    </div>
  );
}

Select.propTypes = propTypes;

export default Select;
