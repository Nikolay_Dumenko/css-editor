const config = {
  filename: 'bundle.js',
  port: 4242,
  path: {
    entry: `${__dirname}/src/index.js`,
    output: `${__dirname}/public`,
    contentBase: `${__dirname}/public`,
  },

  alias: {
    components: `${__dirname}/src/components`,
    containers: `${__dirname}/src/containers`,
    constants: `${__dirname}/src/constants`,
    reducers: `${__dirname}/src/reducers`,
    actions: `${__dirname}/src/actions`,
    config:  `${__dirname}/src/config`,
    routes: `${__dirname}/src/routes`,
    utils: `${__dirname}/src/utils`,
    sagas: `${__dirname}/src/sagas`,
    api: `${__dirname}/src/api`,
    src: `${__dirname}/src`,
  },
};

module.exports = (env, options) => {
  console.log('mode: ', options.mode);
  console.log('env: ', env);

  return {
    mode: options.mode,
    context: __dirname,
    entry: config.path.entry,
    output: {
      path: config.path.output,
      filename: config.filename,
    },
  
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: "eslint-loader",
        },
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.styl$/,
          use: [
            "style-loader",
            "css-loader",
            "stylus-loader"
          ]
        },
      ]
    },

    resolve: {
      modules: ['node_modules'],
      extensions: ['*', '.js', '.jsx'],
      alias: config.alias,
    },

    stats: true,
  
    devServer: {
      open: true,
      historyApiFallback: true,
      port: config.port,
      contentBase: config.path.contentBase
    }
  };
};
